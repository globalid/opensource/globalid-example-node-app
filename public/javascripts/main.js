'use strict';

import { gid } from 'globalid-web-client-launcher';
var n = require('nonce')();
var nonceFunc = n();

const defaultConfig = {
  client_id: process.env.CLIENT_ID,
  default_oauth_options: {
    grant_type: 'code',
    state: 'this is a demo state',
  },
  gid_connect_url: process.env.GID_CONNECT_URL,
  api_base_url: process.env.GID_BASE_URL,
  gid_connect_base_url: process.env.GID_AUTH_URL,
  agencies_url: process.env.AGENCIES_URL
};

const configAppChat = {
    scopes: ['public'],
    redirect_url: process.env.REDIRECT_URL,
    ...defaultConfig,
};

const configAppXYZ = {
    scopes: ['openid'],
    redirect_url: process.env.REDIRECT_URL,
    acrc_uuid: process.env.ACRC_XYZ,
    nonce: nonceFunc.toString(),
    ...defaultConfig,
};

const configCashMoney = {
    scopes: ['openid'],
    redirect_url: process.env.REDIRECT_URL,
    acrc_uuid: process.env.ACRC_CACHE_MONEY,
    nonce: nonceFunc.toString(),
    ...defaultConfig,
};

const configUpgrade = {
    scopes: ['openid'],
    redirect_url: process.env.REDIRECT_URL,
    acrc_uuid: process.env.ACRC_UPGRADE,
    nonce: nonceFunc.toString(),
    ...defaultConfig,
};

const config = {
    appChat: configAppChat,
    xyz: configAppXYZ,
    cashMoney: configCashMoney,
    upgrade: configUpgrade
};

function getQueryParam () {
    const url = new URL(window.location.href);
    return url.searchParams.get('pageName');
}

function handleErrors (payload) {
    console.log(JSON.stringify(payload, null, 2));
}

gid.onError(handleErrors);
const globalidLogin = document.getElementById('globalid-login');
if (globalidLogin != null){
    document.getElementById('globalid-login').onclick = (e) => {
        e.preventDefault();
        const configName = getQueryParam();
        gidLoginInit(config[configName]);
    };
}

const upgradeButton = document.getElementById('upgrade-button');
if (upgradeButton != null){
    document.getElementById('upgrade-button').onclick = (e) => {
        e.preventDefault();
        gidLoginInit(config.upgrade);
    };
}

const onCompleteHandler = async function (webClientResponse) {
    const configName = getQueryParam();
    window.location.href = window.location.origin+'/auth?code='+webClientResponse.code+'&service='+configName;
};

function gidLoginInit(config) {
    gid.init(config);
    gid.login();
    gid.onComplete(onCompleteHandler);
}
