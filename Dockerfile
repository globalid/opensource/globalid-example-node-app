FROM globalid/node-alpine:10.18.0
ENV NODE_ENV production
ADD package.json /tmp/package.json
RUN apk update && apk upgrade && apk add --no-cache bash git openssh curl alpine-sdk
RUN apk --no-cache add --virtual native-deps g++ gcc libgcc libstdc++ linux-headers make python
RUN cd /tmp && npm install && rm -f .npmrc
RUN apk del native-deps

RUN mkdir -p /opt/project && cp -a /tmp/node_modules /opt/project/ && cp -a /tmp/package.json /opt/project/

WORKDIR /opt/project
ADD . .
EXPOSE 9800
USER node

CMD npm start
