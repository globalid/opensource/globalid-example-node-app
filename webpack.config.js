const path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: './public/javascripts/main.js',
    plugins: [
        new Dotenv()
    ],
    output: {
        path: path.resolve('./public/', 'build'),
        filename: 'bundle.js'
    }
};

