const UserService = require("../services/UserService");
const AttestationService = require("../services/AttestationService");
const express = require('express');
const {getTokensForThePII} = require("../utils");
const router = express.Router();

router.get('/', async function (req, res) {
    let access_token = await UserService.getUserAccessToken(req.query.code);
    access_token = access_token.data;
    const user = (await UserService.getUser(access_token.access_token)).data;
    let shared_values = [];
    let PII = [];
    if (access_token.id_token !== null) {
        shared_values = await getTokensForThePII(access_token.id_token);
        if (shared_values.length > 0) {
            const response = await UserService.getPersonalInformation(shared_values, access_token);
            PII = response.PII;
        }
    }

    let localUser = await UserService.getUserByUsername(user.gid_name);
    if (!localUser) {
        localUser = await UserService.createUser({username: user.gid_name})
    }

    for (const value of PII) {
        const attestation = await AttestationService.findAttestationByAttUUID(value.attestation_uuid);
        if (attestation) {
            continue;
        }

        await AttestationService.createAttestation({
            user_id: localUser.id,
            attestation_uuid: value.attestation_uuid,
            type: value.type,
            name: replaceAndCapitalize(value.type),
            value: value.value,
            service: req.query.service,
        });
    }

    req.session.service_key = req.query.service;
    req.session.email = user.gid_name;
    req.session.user_id = localUser.id;
    res.redirect('https://' + req.headers.host + "/auth/profile");
});

router.get('/profile', async function (req, res) {
    const username = req.session.email;
    if (!username){
        return res.redirect('https://' + req.headers.host + "/");
    }

    const attestation = await AttestationService.findAttestationByTypeAndUser("passport_number", req.session.user_id);
    let showUpgrade = true;
    if (attestation){
        showUpgrade = false
    }

    const key = req.session.service_key;
    const services = { xyz: "XYZ Insurance", appChat: "AppChat", cashMoney: "MoneyWallet" };
    req.session.service = services[key];
    const service = services[key];
    const user = await UserService.getUserByUsername(username);
    const attestations = await AttestationService.findAll(user.id);

    return res.render("profile", {
        attestations,
        user,
        service,
        showUpgrade
    })
});

router.get('/logout', async function (req, res) {
    const service = req.session.service;
    req.session.destroy();

    return res.render("index", {
        service
    })
});

function replaceAndCapitalize(s) {
    if (typeof s !== 'string') return '';
    s =  s.charAt(0).toUpperCase() + s.slice(1);

    return s.replace(/_/g, " ");
}

module.exports = router;
