const axios = require('axios');
const {decryptingPIIData, decryptPIIPassword} = require("../utils");
const models  = require('../models');

/**
 * Method to retrieve user data
 * @param code - clients access token
 */
async function getUserAccessToken(code) {
    try {
        return await axios.request({
            method: 'post',
            url: '/auth/token',
            baseURL: process.env.GID_API_BASE_URL,
            headers: {'Content-Type': 'application/json'},
            data: {
                client_id: process.env.CLIENT_ID,
                client_secret: process.env.CLIENT_SECRET,
                redirect_uri: process.env.REDIRECT_URL,
                code: code,
                grant_type: 'authorization_code'
            }
        });
    }catch (e) {
        console.log('Error message:' + e.message)
    }
}

/**
 * Method to retrieve user data
 * @param accessToken
 */
async function getUser(accessToken) {
    try {
        return await axios.request({
            method: 'get',
            url: '/identities/me',
            baseURL: process.env.GID_API_BASE_URL,
            headers: {'Content-Type': 'application/json', 'Authorization': 'bearer ' + accessToken}
        });
    }catch (e) {
        console.log('Error message:' + e.message)
    }
}

/**
 * Method to retrieve user PII values
 * @param access_token - clients access token
 * @param tokens -tokens for the vault
 */
async function getPIIValues(access_token, tokens) {
    return axios.request(`${process.env.GID_API_BASE_URL}/vault/get-encrypted-data`, {
        method: 'post',
        data: {
            private_data_tokens: tokens,
        },
        headers: {
            'Authorization': `Bearer ${access_token}`,
        },
        json: true,
    })
}

/**
 * Get personal information from the vault
 */
async function getPersonalInformation (shared_values, access_token) {
    let PII = [];
    const filePII = [];
    let images = [];
    if (shared_values.length > 0) {
        const access_token = (await getAccessToken()).data;
        const values = await getPIIValues(
                access_token.access_token,
                shared_values,
            );

        PII = await Promise.all(values.data.map(async (value) => {
            const password = await decryptPIIPassword(value.encrypted_data_password);
            const pii = await decryptingPIIData(password, value.encrypted_data);

            if (value.private_file_token !== undefined) {
                filePII.push({
                    password,
                    pii,
                    private_file_token: value.private_file_token,
                })
            }

            return pii
        }));

        images = await getPIIAttachments(filePII, access_token)
    }

    return {
        PII,
        images,
        shared_values,
    }
}

/**
 * Getting token with clients credentials
 */
async function getAccessToken() {
    try {
        return await axios.request({
            method: 'post',
            url: '/auth/token',
            baseURL: process.env.GID_API_BASE_URL,
            headers: {'Content-Type': 'application/json'},
            data: {
                client_id: process.env.CLIENT_ID,
                client_secret: process.env.CLIENT_SECRET,
                grant_type: 'client_credentials',
            }
        });
    }catch (e) {
        console.log('Error message:' + e.message)
    }
}

/**
 * Getting database user
 */
async function getUserByUsername(username) {
    return await models.User.findOne({
        where: {username}}
    );
}

/**
 * Getting database user
 */
async function createUser(data) {
    return await models.User.create(data);
}

/**
 * pipes files to the response
 */
async function getPIIAttachments (attachments, access_token){
    return Promise.all(attachments.map(async (attachment) => {
        const piiValue = JSON.parse(attachment.pii.value);
        const devurl = `${process.env.GID_API_BASE_URL}/vault/attachment/${attachment.private_file_token}/client`;

        return new Promise((resolve, reject) => {
            const chunks = [];
            axios.request(devurl, {
                method: 'get',
                headers: {
                    Authorization: `Bearer ${access_token}`,
                },
            }).pipe(crypto.default.aes.decryptStream(attachment.password))
                .on('data', (chunk) => {
                    chunks.push(chunk)
                })
                .on('error', reject)
                .on('finish', () => {
                    resolve(`data:${piiValue.mime_type};base64,${Buffer.concat(chunks).toString('base64')}`)
                })
        })
    }))
}

module.exports = { getUser, getUserAccessToken, getAccessToken, getPersonalInformation, getUserByUsername, createUser };
