const models  = require('../models');

/**
 * Getting attestation by att_uuid
 */
async function findAttestationByAttUUID(attestation_uuid) {
    return await models.Attestations.findOne({
        where: {attestation_uuid}
    })
}

/**
 * Create attestation
 */
async function createAttestation(data) {
    return await models.Attestations.create(data);
}

/**
 * Find attestations
 */
async function findAll(user_id) {
    return await models.Attestations.findAll({
        where: { user_id }
    });
}

/**
 * Getting attestation by type
 */
async function findAttestationByTypeAndUser(type, user_id) {
    return await models.Attestations.findOne({
        where: {type, user_id}
    })
}

module.exports = { findAttestationByAttUUID, createAttestation, findAll, findAttestationByTypeAndUser };
