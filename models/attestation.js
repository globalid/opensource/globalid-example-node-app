'use strict';
module.exports = (sequelize, DataTypes) => {
    let Attestation = sequelize.define('Attestations', {
        attestation_uuid: DataTypes.STRING,
        user_id: DataTypes.INTEGER,
        type: DataTypes.STRING,
        name: DataTypes.STRING,
        value: DataTypes.STRING,
        service: DataTypes.STRING
    }, {
        timestamps: false
    });

    Attestation.associate = function (models) {
        models.Attestations.belongsTo(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'user_id',
                allowNull: false
            }
        });
    };

    return Attestation;
};
