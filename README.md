### globaliD integration application example
A globaliD integration demo using NodeJS/Express application

Live: [globalid-example-node.herokuapp.com](https://globalid-example-node.herokuapp.com)


Overview
--------

This is a minimal forecast-making app. It uses 
[Express web framework for Node.js][express] and the [globalid-web-client-launcher] to
enable users to authenticate and provide verifications using the globaliD ecosystem.

[express]: https://expressjs.com/
[globalid-web-client-launcher]: https://www.npmjs.com/package/globalid-web-client-launcher

Dependencies
------------

- Node.js
- Express
- Sqlite

Walkthrough
-----------

copy `.env-example` to `.env` and fill in the missing environmental variables.

#### Create a globaliD app and client ID

First create a globaliD developer app [globaliD developer dashboard][gid-newapp]
and note the client and secret.

[gid-newapp]: https://developer.global.id/app/developer/new

```shell
GLOBALID_CLIENT_ID=xxxx
GLOBALID_CLIENT_SECRET=xxxx
```

### Basic app setup

##### 1. Create an application skeleton
```shell
npx express-generator --view=pug
```

##### 2. Install dependencies
```shell
npm install
```

##### 3. Install sequelize and sqlite3
```shell
npm install sequelize sequelize-cli sqlite3
# generate models
node_modules/.bin/sequelize init
node_modules/.bin/sequelize model:create --name User --attributes username:string
```

##### 4. Sync DB models on startup
If you want to use the automatic table creation that sequelize provides, you have to adjust the bin/www file to this:
   
```javascript

var http = require('http');
var models = require("../models");

// sync() will create all tables if they doesn't exist in database
models.sequelize.sync().then(function () {
  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);
});

```  

##### 5. Specify db dialect
Open config/config.json file and change db dialect 
```javascript
{ dialect: "sqlite" }
```

##### 6. Add main.js
Create empty main.js in `/public/javascript/`

##### 7. Enable JS6
Create webpack.config.js file on root path of project 
`touch webpack.config.js` and add webpack config 

```javascript
const path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: './public/javascripts/main.js',
    plugins: [
        new Dotenv()
    ],
    output: {
        path: path.resolve('./public/', 'build'),
        filename: 'bundle.js'
    }
};
  
```

##### 8. Run and install webpack 
```shell
npm install webpack --save-dev
npm install dotenv-webpack --save-dev
```

and for building your main.js you will need to run this command (or add it to `package.json`)

```shell
node_modules/webpack/bin/webpack.js --config webpack.config.js
```

##### 9. Ngrok
Note that, for security reasons, the redirect URL must be included in the list of whitelisted redirect URLs 
for your app. You can set these up within the globaliD Developer Portal. If you attempt to use a different URI, it will be rejected.

`Due to security restrictions, the redirect URI must use HTTPS. This means that you cannot simply run a web server
 on localhost to test globaliDConnect on your development machine — the attempt to redirect to a URL 
 such as http://localhost:8080 will be rejected. To make this work, you can use a tool such as ngrok to 
 make your localhost-based web server accessible via HTTPS.
 `

##### 10. Verify our setup
After running command 8 you should have created `dist` folder inside /public directory, \
inside `dist` should exist `bundle.js` for making it work with node js 
open this `/views/index.pug` file and on the end of the file add this line 
`script(src='/dist/bundle.js')`, and for checking is it worked with you node app 
go to `main.js` and `alert('hello');`


### globaliD integration

##### 1. Add globalid-web-client-launcher npm 
```shell
npm install --save globalid-web-client-launcher
```

##### 2. Add this code to main.js 
```javascript
import { gid } from 'globalid-web-client-launcher';

function handleOutput (payload) {
  console.log(JSON.stringify(payload, null, 2));
}

const config = {
  client_id: 'CLIENT_ID_GOES_HERE',
  acrc_uuid: '0f1a04fa-abcc-4427-bd9e-7cd10f1d4904',
  default_oauth_options: {
    grant_type: 'code',
    state: 'this is a demo state',
  },
  version: '*',
  redirect_url: 'https://localhost:3000/auth',
  scopes: ['public'],
  gid_connect_url: 'https://dev-auth.global.id/localid/popup.html',
  api_base_url: 'https://dev-api.gidstaging.net',
  gid_connect_base_url: 'https://dev-auth.global.id',
  agencies_url: 'https://dev-agencies.gidstaging.net'
};

gid.init(config);
gid.onComplete(handleOutput);
gid.onError(handleOutput);

document.getElementById('button-login').onclick = () => {
  gid.login();
};

```

##### 3. Add auth callback
Create `routes/auth.js` and paste this
```javascript
const express = require('express');
const models  = require('../models');
const router  = express.Router();

router.get('/', async function(req, res) {
  const accessToken = await helper.getAccessToken(req.query.code);
  const user = await helper.getUser(accessToken.data.access_token);

  res.redirect('/');
});

module.exports = router;
```

##### 4. Heroku Deployment

This app is set up to deploy on Heroku. You will have to add your environmental variables to Heroku.